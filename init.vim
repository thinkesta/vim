call plug#begin()
Plug 'morhetz/gruvbox'
Plug 'preservim/nerdtree'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'
Plug 'nvim-treesitter/nvim-treesitter'
Plug 'BurntSushi/ripgrep'
Plug 'sharkdp/fd'
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'deoplete-plugins/deoplete-go', { 'do': 'make'}
Plug 'tpope/vim-fugitive'
Plug 'vim-airline/vim-airline'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'diepm/vim-rest-console'
Plug 'ThePrimeagen/git-worktree.nvim'
Plug 'diepm/vim-rest-console'
Plug 'voldikss/vim-floaterm'
Plug 'sheerun/vim-polyglot'
Plug 'leafOfTree/vim-svelte-plugin'
call plug#end()


" brew install ripgrep
" curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
" CocInstall coc-json coc-tsserver
" CocInstall https://github.com/shashank-sachan/coc-go@1.1.2
" CocInstall coc-prettier
" CocInstall coc-groovy
" CocInstall coc-graphql
" sdk install java 8.0.322-zulu
" :GoInstallBinaries
" :GoUpdateBinaries gopls

" copy complete function va{Vy
" select lines and Gclog
" Ctrl+c + Shift+i => // => Esc

let mapleader = " "
filetype plugin indent on

set cursorline
set autowrite
set exrc
set guicursor=
set clipboard+=unnamedplus
set number relativenumber
set nohlsearch
set hidden
set noerrorbells
set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
set nowrap
set incsearch
set scrolloff=8
set noshowmode
set completeopt=menuone,noinsert,noselect
set signcolumn=yes
" set termguicolors

imap jj <Esc>
autocmd FileType javascript setlocal shiftwidth=2 tabstop=2 softtabstop=2 expandtab

" Theme
autocmd vimenter * ++nested colorscheme gruvbox
colorscheme gruvbox
set background=dark

" format node code using coc command
noremap	<leader>jf :CocCommand prettier.formatFile <cr>

" Rest Client
noremap <silent>http :CocCommand rest-client.request <cr>

" Telescope
nnoremap <silent>ff <cmd>Telescope find_files hidden=true<cr>
nnoremap <silent>fg <cmd>Telescope live_grep hidden=true<cr>
nnoremap <silent>fb <cmd>Telescope buffers<cr>
nnoremap <silent>fh <cmd>Telescope help_tags<cr>

" NERD Tree
nnoremap <silent>nt <cmd>NERDTree<cr>
nnoremap <silent>ntt <cmd>NERDTreeToggle<cr>

" vim airline
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#buffer_nr_show = 1

" Common Go commands
au FileType go nmap <silent>ec <cmd>GoErrCheck<cr>
au FileType go nmap <silent>gr <Plug>(go-run)
au FileType go nmap <silent>gb <Plug>(go-build)
au FileType go nmap <silent>gt <Plug>(go-test)
au FileType go nmap <silent>gc <Plug>(go-coverage-toggle)
au FileType go nmap <silent>ge <Plug>(go-rename)
au FileType go nmap <silent>gs <Plug>(go-implements)
au FileType go nmap <silent>gi <Plug>(go-info)

" Navigation commands
au FileType go nmap <silent>ds <Plug>(go-def-split)
au FileType go nmap <silent>dv <Plug>(go-def-vertical)

" Alternate commands
au FileType go nmap <silent>ae <Plug>(go-alternate-edit)
au FileType go nmap <silent>av <Plug>(go-alternate-vertical)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Go syntax highlighting
let g:go_highlight_fields = 1
let g:go_highlight_functions = 1
let g:go_highlight_function_calls = 1
let g:go_highlight_extra_types = 1
let g:go_highlight_operators = 1
let g:go_doc_popup_window = 1

" Auto formatting and importing
let g:go_fmt_autosave = 1
let g:go_fmt_command = "goimports"

" Status line types/signatures
let g:go_auto_type_info = 1

" let g:deoplete#enable_at_startup = 1

" git worktree
nnoremap <silent><C-L> <cmd>lua require('telescope').extensions.git_worktree.git_worktrees()<cr>
nnoremap <silent><C-C> <cmd>lua require('telescope').extensions.git_worktree.create_git_worktree()<cr>

" resize remap
nnoremap <leader>+ <cmd>vertical resize +5<cr>
nnoremap <leader>- <cmd>vertical resize -5<cr>

" svelte
let g:vim_svelte_plugin_use_typescript = 1
let g:vim_svelte_plugin_has_init_indent = 1
